Rails.application.routes.draw do
  namespace :api do
    resources :users, only: [:index, :show] do
      resources :repositories, only: :index
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
