class Consumer
  API_PATH = "https://api.github.com"
  attr_accessor :users, :user, :repositories
  def initialize
    self.users = []
    self.repositories = []
    self.user = nil
  end

  def find_users
    uri = URI(get_next_users_url)
    response = Net::HTTP.get(uri)
    self.users = JSON.parse(response)
    {
      users: self.users,
      next_page: get_next_users_url
    }
  end

  def find_user(username)
    uri = URI("#{API_PATH}/users/#{username}")

    response = Net::HTTP.get(uri)
    self.user = JSON.parse(response)
  end

  def find_repositories(username)
    uri = URI("#{API_PATH}/users/#{username}/repos")

    response = Net::HTTP.get(uri)
    self.repositories = JSON.parse(response)
  end

  private
  def get_next_users_url
    url = "#{API_PATH}/users"
    url << "?since=#{self.users.last["id"]}" unless self.users.empty?
    url
  end
end
