require 'rails_helper'

RSpec.describe Api::RepositoriesController, type: :request do

  describe "GET /index" do
    before do
      get api_user_repositories_path('rodrigotoledo')
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns json body" do
      content = File.read(File.join('spec','support','fixtures','repos.json'))
      content_json = JSON.parse(content, quirks_mode: true)
      expect(JSON.parse(response.body)).to eql(content_json)
    end

  end

end
