require 'rails_helper'

RSpec.describe Api::UsersController, type: :request do

  describe "GET /index" do
    before do
      get api_users_path
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns users json body" do
      content = File.read(File.join('spec','support','fixtures','users.json'))
      content_json = JSON.parse(content, quirks_mode: true)
      users_body = JSON.parse(response.body)
      expect(users_body["users"]).to eql(content_json)
    end
  end

  describe "GET /show" do
    before do
      get api_user_path('rodrigotoledo')
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns json body" do
      content = File.read(File.join('spec','support','fixtures','user.json'))
      content_json = JSON.parse(content, quirks_mode: true)
      expect(JSON.parse(response.body)).to eql(content_json)
    end
  end
end
