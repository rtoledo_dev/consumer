require 'rails_helper'
require 'consumer'

describe Consumer do
  describe "#initializing" do
    context "with empty attributes" do
      it "have default values" do
        expect(subject.users).to be_empty
        expect(subject.repositories).to be_empty
        expect(subject.user).to be_nil
      end
    end
  end
end
