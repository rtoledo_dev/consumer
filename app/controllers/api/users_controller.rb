class Api::UsersController < ApplicationController
  include ConsumerConcern

  def index
    render json: @consumer.find_users, status: :ok
  end

  def show
    render json: @consumer.find_user(params[:id]), status: :ok
  end
end
