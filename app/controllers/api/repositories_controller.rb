class Api::RepositoriesController < ApplicationController
  include ConsumerConcern

  def index
    render json: @consumer.find_repositories('rodrigotoledo'), status: :ok
  end
end
