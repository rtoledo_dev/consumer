module ConsumerConcern
  extend ActiveSupport::Concern
  included do
    before_action :set_consumer
  end

  protected
    def set_consumer
      @consumer ||= Consumer.new
    end
end
