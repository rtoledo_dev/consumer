# README

This project consumes github api and provide data to users

- Dependencies
  - Docker
  - Ruby 2.>
  - Internet, keyboard, fingers and screen
  - Head

## Development helpers

- Environment setup, code everywhere

`docker-compose build`
`docker-compose up`

- First steps
  
`docker-compose run web bundle exec rails db:create db:migrate db:seed`

if you need shutdown the server

`docker-compose down`

- Code testing awalys!

`docker-compose run web bundle exec guard`

Need run one test?!

`docker-compose run web bundle exec rspec ./spec/your/test`

## Staging (Heroku) helpers

Publishing

Commit your stuff in master environment and run command

- git push heroku master
